package com.blo.bbcapp;

import com.blo.bbcapp.Controller.CheckOnlineConnectivity;
import com.blo.bbcapp.Controller.FetchFruitsList;
import com.blo.bbcapp.Controller.IoC;
import com.blo.bbcapp.Controller.SendDisplayEvents;
import com.blo.bbcapp.Model.Fruit;
import com.blo.bbcapp.Model.MyGridViewAdapter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UnitTests {
    Fruit original = new Fruit("original", 5.0, 10.0);
    CheckOnlineConnectivity checkOnlineConnectivity = new CheckOnlineConnectivity();
    FetchFruitsList fetchFruitsList = new FetchFruitsList(null, "", null, null);
    SendDisplayEvents sendDisplayEvents = new SendDisplayEvents();


    //Fruit Tests
    @Test
    public void testEqual() {
        Fruit expected = new Fruit("Apple", 5.0, 10.0);
        assertTrue(original.equals(original));
        assertFalse(original.equals(expected));
        assertFalse(original.equals(""));
    }


    //Online Connectivity
    @Test(expected = NullPointerException.class)
    public void onlineConnectivitytest1() throws NullPointerException {
        checkOnlineConnectivity.isNetworkAvailable(null);
    }

    //FetchFruitsList Tests
    @Test
    public void testFetchFruitsList1() {
        FetchFruitsList fetchFruitsList2 = new FetchFruitsList(null, "", null, null);
        assertNotEquals(fetchFruitsList, fetchFruitsList2);

    }

    @Test(expected = NullPointerException.class)
    public void testFetchFruitsList2() throws Exception {
        fetchFruitsList.fetch();
    }

    //SendDisplayEvents Tests
    @Test(expected = ExceptionInInitializerError.class)
    public void testSendDisplayEvents1() {
        sendDisplayEvents.sendDisplay(null, null, 0L);
    }

    @Test(expected = NoClassDefFoundError.class)
    public void testSendDisplayEvents2() {
        sendDisplayEvents.sendDisplay(null, IoC.eventsUrl, 0L);

    }

    //MyGridViewAdapter Tests
    @Test(expected = NumberFormatException.class)
    public void test0MyGridViewAdapter() {
        List<Fruit> list = new ArrayList<Fruit>();
        list.add(original);
        MyGridViewAdapter myGridViewAdapter = new MyGridViewAdapter(null, list);
        myGridViewAdapter.getItem(Integer.parseInt("String"));

    }


}