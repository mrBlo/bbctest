package com.blo.bbcapp.Controller;

public class IoC {

    public static CheckOnlineConnectivity checkOnlineConnectivity = new CheckOnlineConnectivity();
    public static SendLoadEvents sendLoadEvents = new SendLoadEvents();
    public static SendErrorEvent sendErrorEvent = new SendErrorEvent();
    public static SendDisplayEvents sendDisplayEvents = new SendDisplayEvents();
    public static String urlAddress = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/data.json";
    public static String eventsUrl = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats";

    public IoC() {
    }

}
