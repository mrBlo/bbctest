package com.blo.bbcapp.Controller;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.blo.bbcapp.Model.Fruit;
import com.blo.bbcapp.Model.MyErrorTracker;
import com.blo.bbcapp.Model.MyGridViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FetchFruitsList {
    private Context context;
    private String urlAddress;
    private GridView rv;
    private ProgressBar progressBar;
    private Fruit fruit;
    private ArrayList<Fruit> fruitList = new ArrayList<>();
    private String sourceActivity = "FetchFruitsList class";
    private long mRequestStartTime;


    public FetchFruitsList(Context context, String urlAddress, GridView rv, ProgressBar progressBar) {
        this.context = context;
        this.urlAddress = urlAddress;
        this.rv = rv;
        this.progressBar = progressBar;
        //this.aQuery = aQuery;
    }

    public void fetch() {
//making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
        mRequestStartTime = System.currentTimeMillis(); // set the request start time just before you send the request.
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlAddress,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // calculate the duration in milliseconds
                        long totalRequestTime = System.currentTimeMillis() - mRequestStartTime;
                        //send Request time here to server
                        sendRequestTime(context, IoC.eventsUrl, totalRequestTime);

                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject jsonObject = new JSONObject(response);

                            //we have the array named fruit inside the object
                            //so here we are getting that json array
                            JSONArray jsonArray = jsonObject.getJSONArray("fruit");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                String type = jsonObject.getString("type");
                                Log.d("type", type + "");
                                Double price = jsonObject.getDouble("price");
                                Log.d("price", price + "");
                                Double weight = jsonObject.getDouble("weight");
                                Log.d("weight", weight + "");

                                //link JSONObject to fruit object
                                fruit = new Fruit(type, price, weight);
                                fruitList.add(fruit);

                            }
                            System.out.println(fruitList);
                            //Bind to Adapter
                            rv.setAdapter(new MyGridViewAdapter(context, fruitList));
                            progressBar.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //send error event to server here
                            IoC.sendErrorEvent.sendError(context, IoC.eventsUrl, e.getMessage(), sourceActivity);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // calculate the duration in milliseconds
                        long totalRequestTime = System.currentTimeMillis() - mRequestStartTime;
                        //send Request time here to server
                        sendRequestTime(context, IoC.eventsUrl, totalRequestTime);

                        //displaying the error in toast if occurs
                        if (error instanceof NoConnectionError) {
                            //This indicates that there is no connection
                            Toast.makeText(context, "" + MyErrorTracker.IO_EROR, Toast.LENGTH_SHORT).show();
                            //new SendErrorEvent(context,eventsUrl,error.getMessage(),sourceActivity).sendError();
                            handleError(context, error, IoC.eventsUrl, sourceActivity);

                        } else if (error instanceof TimeoutError) {
                            //This indicates that the request has timed out
                            Toast.makeText(context, "" + MyErrorTracker.TIMEOUT_ERROR, Toast.LENGTH_SHORT).show();
                            handleError(context, error, IoC.eventsUrl, sourceActivity);
                        } else if (error instanceof AuthFailureError) {
                            //Error indicating that there was an Authentication Failure while performing the request
                            Toast.makeText(context, "" + MyErrorTracker.AUTH_FAILURE_ERROR, Toast.LENGTH_SHORT).show();
                            handleError(context, error, IoC.eventsUrl, sourceActivity);

                        } else if (error instanceof ServerError) {
                            //Indicates that the server responded with a error response
                            Toast.makeText(context, "" + MyErrorTracker.SERVER_ERROR, Toast.LENGTH_SHORT).show();
                            //send error event to server here
                            handleError(context, error, IoC.eventsUrl, sourceActivity);

                        } else if (error instanceof NetworkError) {
                            //Indicates that there was network error while performing the request
                            Toast.makeText(context, "" + MyErrorTracker.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                            //send error event to server here
                            handleError(context, error, IoC.eventsUrl, sourceActivity);

                        } else if (error instanceof ParseError) {
                            // Indicates that the server response could not be parsed
                            Toast.makeText(context, "" + MyErrorTracker.PARSE_ERROR, Toast.LENGTH_SHORT).show();
                            handleError(context, error, IoC.eventsUrl, sourceActivity);

                        } else {
                            //Any other error
                            Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                            handleError(context, error, IoC.eventsUrl, sourceActivity);
                        }

                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        //adding the string request to request queue
        requestQueue.add(stringRequest);


    }


    //Handle Error Method
    private void handleError(Context context, VolleyError error, String eventsUrl, String sourceActivity) {
        //send error event to server here
        IoC.sendErrorEvent.sendError(context, eventsUrl, error.getMessage(), sourceActivity);

    }

    //Handle Request Load Method

    private void sendRequestTime(Context context, String eventsUrl, long totalRequestTime) {
        //send Request Time
        IoC.sendLoadEvents.sendLoad(context, eventsUrl, totalRequestTime, "Fetching Fruits");
    }

}
