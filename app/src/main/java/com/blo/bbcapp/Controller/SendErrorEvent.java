package com.blo.bbcapp.Controller;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class SendErrorEvent {
    //Send Error Events to Server
    public SendErrorEvent() {
    }


    public void sendError(final Context c, String urlAddress, String errorMessage, String sourceActivity) {
//making the progressbar visible
        // progressBar.setVisibility(View.VISIBLE);
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                urlAddress + "?event=error" + "&data=" + errorMessage + " located in " + sourceActivity,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        //  progressBar.setVisibility(View.INVISIBLE);
                        //Toast.makeText(c, ""+response, Toast.LENGTH_LONG).show();
                        System.out.println("RESPONSE--------> " + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(c, error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(c);

        //adding the string request to request queue
        requestQueue.add(stringRequest);


    }

}
