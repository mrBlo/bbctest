package com.blo.bbcapp.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.blo.bbcapp.Controller.FetchFruitsList;
import com.blo.bbcapp.Controller.IoC;
import com.blo.bbcapp.Model.Fruit;
import com.blo.bbcapp.Model.MyErrorTracker;
import com.blo.bbcapp.Model.SessionManager;
import com.blo.bbcapp.R;

public class MainActivity extends AppCompatActivity {

    AQuery aQuery;
    SwipeRefreshLayout mSwipeRefreshLayout;
    SessionManager sessionManager;
    private GridView gridView;
    private Fruit selectedFruit;
    private Intent intent;
    private Bundle bundle;
    private Context context = MainActivity.this;
    private long screenRequestStartTime;
    private ProgressBar ProgressBarLoading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Display Swipe Toast
        Toast.makeText(context, "Swipe Down to Refresh", Toast.LENGTH_SHORT).show();
        //find views
        gridView = findViewById(R.id.fruits_gridView);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);


        ProgressBarLoading = findViewById(R.id.progressBar);
        aQuery = new AQuery(this);
        sessionManager = new SessionManager(context);


        //Handling Swipe Refresh Layout
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light);

        //check for Internet connectivity
        if (!IoC.checkOnlineConnectivity.isNetworkAvailable(context)) {
            Toast.makeText(context, "" + MyErrorTracker.IO_EROR, Toast.LENGTH_SHORT).show();
            //hiding the progressbar after completion
            ProgressBarLoading.setVisibility(View.INVISIBLE);
        } else {
            //Fetch fruits
            new FetchFruitsList(context, IoC.urlAddress, gridView, ProgressBarLoading).fetch();

        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();

            }
        });

        //on click of GridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // set the request start time just after on click.
                screenRequestStartTime = System.currentTimeMillis();
                selectedFruit = (Fruit) parent.getItemAtPosition(position);
                //moving to next page
                intent = new Intent(context, FruitActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("BundleObject", selectedFruit);
                //save Request Time to SessionManager
                // intent.putExtra("screenRequestStartTimeBundle", screenRequestStartTime);
                //set to O first
                sessionManager.setTotalDisplayRequestTime(0);
                sessionManager.setTotalDisplayRequestTime(screenRequestStartTime);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


    }

    //onBackPressed
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit this application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //set to O
                        sessionManager.setTotalDisplayRequestTime(0);
                        finishAndRemoveTask();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void refreshContent() {
        // showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);
        //Fetch fruits
        if (!IoC.checkOnlineConnectivity.isNetworkAvailable(context)) {
            Toast.makeText(context, "" + MyErrorTracker.IO_EROR, Toast.LENGTH_SHORT).show();

            // To keep animation for 3 seconds
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ProgressBarLoading.setVisibility(View.INVISIBLE);
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, 3000); // Delay in millis


        } else {
            // To keep animation for 2 seconds
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Stop animation (This will be after 1 second)
                    new FetchFruitsList(context, IoC.urlAddress, gridView, ProgressBarLoading).fetch();
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(context, "Refreshed", Toast.LENGTH_SHORT).show();
                }
            }, 2000); // Delay in millis


        }
    }


    @Override
    protected void onResume() {
//        Toast.makeText(context, "On Resume", Toast.LENGTH_SHORT).show();
//get Screen Request Start time
        long screenRequestStartTime = sessionManager.getTotalDisplayRequestTime();
        // calculate the duration in milliseconds
        long totalDisplayRequestTime = System.currentTimeMillis() - screenRequestStartTime;
        // Toast.makeText(context, "" + totalDisplayRequestTime, Toast.LENGTH_SHORT).show();
        //send Display Request time here to server
        sendDisplayRequestTime(context, IoC.eventsUrl, totalDisplayRequestTime);
        super.onResume();
    }

    //Handle send Display Request Time Method
    public void sendDisplayRequestTime(Context context, String eventsUrl, long totalDisplayRequestTime) {
        //send Request Time
        IoC.sendDisplayEvents.sendDisplay(context, eventsUrl, totalDisplayRequestTime);
    }


}

