package com.blo.bbcapp.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blo.bbcapp.Controller.IoC;
import com.blo.bbcapp.Model.Fruit;
import com.blo.bbcapp.Model.SessionManager;
import com.blo.bbcapp.R;

import java.util.Objects;

public class FruitActivity extends AppCompatActivity {

    Context context = FruitActivity.this;
    SessionManager sessionManager;
    long screenRequestStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sessionManager = new SessionManager(context);
        //get Bundle obj
        Fruit bundleObject = (Fruit) Objects.requireNonNull(getIntent().getExtras()).get("BundleObject");
        //get Screen Request Start time
        // screenRequestStartTime= getIntent().getExtras().getLong("screenRequestStartTimeBundle");
        long screenRequestStartTime = sessionManager.getTotalDisplayRequestTime();
        // calculate the duration in milliseconds
        long totalDisplayRequestTime = System.currentTimeMillis() - screenRequestStartTime;
        //send Display Request time here to server
        sendDisplayRequestTime(context, IoC.eventsUrl, totalDisplayRequestTime);


        ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading...Please wait");
        pd.show();

        //check for empty bundle
        if (bundleObject.isEmpty(bundleObject)) {
            Toast.makeText(this, "Cannot load fruit", Toast.LENGTH_SHORT).show();
            finish();
        }

        //findViews
        TextView typetextView = findViewById(R.id.type_textView);
        TextView pricetextView = findViewById(R.id.price_textView);
        TextView weighttextView = findViewById(R.id.weight_textView);
        ImageView fruitimageView = findViewById(R.id.fruit_imageView);

        /*       SET OBJECT VALUES TO ALL VIEWS        */
        typetextView.setText(bundleObject.getType().toUpperCase());
        pricetextView.setText(String.format("Price : £%s", bundleObject.getPrice()));
        weighttextView.setText(String.format("Weight : %s kg", bundleObject.getWeight()));

        //cancel progressDialog after setting to views
        pd.cancel();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // set the request start time just after on click.
            long screenRequestStartTime = System.currentTimeMillis();
            //set to O first
            sessionManager.setTotalDisplayRequestTime(0);
            sessionManager.setTotalDisplayRequestTime(screenRequestStartTime);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //on Back Pressed


    @Override
    public void onBackPressed() {
        // set the request start time just after on click.
        long screenRequestStartTime = System.currentTimeMillis();
        //set to O first
        sessionManager.setTotalDisplayRequestTime(0);
        sessionManager.setTotalDisplayRequestTime(screenRequestStartTime);
        super.onBackPressed();
    }

    //Handle send Display Request Time Method
    public void sendDisplayRequestTime(Context context, String eventsUrl, long totalDisplayRequestTime) {
        //send Request Time
        IoC.sendDisplayEvents.sendDisplay(context, eventsUrl, totalDisplayRequestTime);
    }

}
