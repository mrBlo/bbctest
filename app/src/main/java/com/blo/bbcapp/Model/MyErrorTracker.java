package com.blo.bbcapp.Model;

public class MyErrorTracker {


    /*
    Main Responsibility : HELP US TRACK CONNECTION EXCEPTIONS AT RUNTIME
     */
    public final static String WRONG_URL_FORMAT = "Error : Wrong URL Format ";
    public final static String CONNECTION_ERROR = "Error : Unable To Establish Connection ";
    public final static String IO_EROR = "Error : No Internet Connectivity ";
    public final static String RESPONSE_EROR = "Error : Bad Response - ";
    public final static String EMPTY_URL = "Error : No URL Found ";
    public final static String AUTH_FAILURE_ERROR = "Error: Authentication Failed";
    public final static String SERVER_ERROR = "Server Error";
    public final static String NETWORK_ERROR = "Network Error";
    public final static String PARSE_ERROR = "Unable to Parse Server Response";
    public final static String TIMEOUT_ERROR = "Request TimeOut Error ";


}
