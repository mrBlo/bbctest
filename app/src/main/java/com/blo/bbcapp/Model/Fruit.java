package com.blo.bbcapp.Model;

import java.io.Serializable;
import java.util.Objects;

public class Fruit implements Serializable {

    private String type, image;
    private Double price, weight;

    //CONSTRUCTORS
    public Fruit() {
    }

    public Fruit(String type, Double price, Double weight) {
        this.type = type;
        this.price = price;
        this.weight = weight;
    }

    public Fruit(String type, String image, Double price, Double weight) {
        this.type = type;
        this.image = image;
        this.price = price;
        this.weight = weight;
    }

    //toString
    @Override
    public String toString() {
        return "Fruit{" +
                "type='" + type + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                '}';
    }

    //isEmpty
    public boolean isEmpty(Fruit fruit) {
        //Empty fruits have no types
        return fruit.getType().isEmpty();
    }

    //Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fruit)) return false;
        Fruit fruit = (Fruit) o;
        return Objects.equals(getType(), fruit.getType()) &&
                Objects.equals(getPrice(), fruit.getPrice()) &&
                Objects.equals(getWeight(), fruit.getWeight());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getPrice(), getWeight());
    }

    //GETTERS AND SETTERS
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
