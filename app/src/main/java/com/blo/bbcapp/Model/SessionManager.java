package com.blo.bbcapp.Model;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    private static final String PREF_NAME = "BBCdemo";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;
    private int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        super();
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public Long getTotalDisplayRequestTime() {
        //Storing name  in Pref
        return pref.getLong("totalDisplayRequestTime", 0L);
    }

    public void setTotalDisplayRequestTime(long totalDisplayRequestTime) {
        //Storing name  in Pref
        editor.putLong("totalDisplayRequestTime", totalDisplayRequestTime);
        editor.commit();
    }

}
