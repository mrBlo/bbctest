package com.blo.bbcapp.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.blo.bbcapp.R;

import java.util.List;

public class MyGridViewAdapter extends BaseAdapter {
    private Context context;
    private View mygrid;
    private LayoutInflater inflater;
    private Fruit fruit;
    private List<Fruit> fruitList;
    private AQuery aq;

    public MyGridViewAdapter(Context context, List<Fruit> fruitList) {
        this.context = context;
        this.fruitList = fruitList;

    }

    @Override
    public int getCount() {
        return fruitList.size();

    }

    @Override
    public Object getItem(int position) {
        return fruitList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fruit = fruitList.get(position);
        mygrid = new View(context);
        aq = new AQuery(context);

        if (convertView == null) {
            mygrid = inflater.inflate(R.layout.fruits_card_layout, null);
        } else {
            mygrid = convertView;
        }
        //get obj using view position
        TextView typeTextView = mygrid.findViewById(R.id.type_textView);
        ImageView fruitImageView = mygrid.findViewById(R.id.imageView);
        typeTextView.setText(fruit.getType().toUpperCase());

        if (fruit.getImage() != null) {

            //if we are not able to load the image, use a default image (R.drawable.default_image)
            //this image is huge, avoid memory caching
            aq.id(fruitImageView).progress(R.id.progressBar).
                    image(fruit.getImage(), false, true, 0, R.drawable.fruits2, null, AQuery.FADE_IN);

        } else {
            ///default image here
            fruitImageView.setImageResource(R.drawable.fruits2);
        }


        return mygrid;


    }
}
